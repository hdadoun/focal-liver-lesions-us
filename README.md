**Detection, Localization, and Characterization of Focal Liver Lesions in Abdominal US with Deep Learning**

This repository includes Pytorch code used for the paper : Detection, Localization, and Characterization of Focal Liver Lesions in Abdominal US with Deep Learning. 

The different sub-directories were forked directly from, with very few modifications : 


**DEtection-TRansformer :**

https://github.com/facebookresearch/detr 

**FasterRCNN :**

https://github.com/pytorch/vision/blob/main/torchvision/models/detection/

**The aLRP loss :**

https://github.com/kemaloksuz/aLRPLoss

**Augmentations :**

https://github.com/harpalsahota/bbaug