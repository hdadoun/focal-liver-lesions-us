import sys
from collections import Counter
import matplotlib.pyplot as plt
import numpy as np
from typing import Callable, Dict, List, Tuple

import PIL  # type: ignore
import torch  # type: ignore
from PIL import Image  # type: ignore
from torch.utils.data import DataLoader  # type:ignore

from util import utils_auc as T

class InvalidFileFormat(Exception):
    pass


class InvalidBoxLabelFormat(Exception):
    pass


class InvalidFileName(Exception):
    pass

def collate_fn(batch):
    return tuple(zip(*batch))

def get_transform(train):
    transforms = [T.ToTensor()]
    if train:
        transforms.append(T.RandomHorizontalFlip(0.5))
    return T.Compose(transforms)


class DatasetFasterRCNN(torch.utils.data.dataset.Dataset):


    def __init__(self, root: str, path_to_file: str, transforms: Callable = None,train: bool =True):
        self.root = root
        self.path_to_file = path_to_file
        self.imgs, self.boxes, self.labels = get_imgs_boxes_labels(
            self.root, self.path_to_file
        )
        self.transforms = transforms
        self.train = train


    def __getitem__(self, idx: int) -> Tuple[torch.Tensor, Dict[str, torch.Tensor]]:
        img = self.imgs[idx]
        boxes = self.boxes[idx]
        labels = self.labels[idx]
        num_objs = len(labels)
        image_id = torch.tensor([idx])
        iscrowd = torch.zeros((num_objs,), dtype=torch.int64)
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        target = {
            "boxes": boxes,
            "labels": labels,
            "image_id": image_id,
            "area": area,
            "iscrowd": iscrowd,
        }

        if self.transforms is not None:
            img, target = self.transforms(img, target)

        return img, target

    def __len__(self):
        return len(self.imgs)


def get_imgs_boxes_labels(
    root: str, path_to_file: str
) -> Tuple[List[PIL.Image.Image], List[torch.Tensor], List[torch.Tensor]]:
    with open(path_to_file) as f:
        lines = f.readlines()
        imgs = []
        boxes = []
        labels = []
        print(sorted(lines))
        for line in sorted(lines):
            splitted = line.strip().split()
            img_path = root + "data_images/" + splitted[0]
            try:
                img = Image.open(img_path).convert("RGB")
            except FileNotFoundError:
                raise InvalidFileName(
                    "We could not find your image, check the image name given"
                )

            num_objs = (len(splitted) - 1) / 5
            if not num_objs.is_integer():
                raise InvalidFileFormat(
                    "The format for detection should be img_name.jpj xmin ymin xmax ymax label"
                )
            box = []
            label = []
            for i in range(int(num_objs)):
                try:
                    xmin = splitted[1 + 5 * i]
                    ymin = splitted[2 + 5 * i]
                    xmax = splitted[3 + 5 * i]
                    ymax = splitted[4 + 5 * i]
                    box.append([float(xmin), float(ymin), float(xmax), float(ymax)])
                    label.append(int(splitted[5 + 5 * i]))
                except ValueError:
                    raise InvalidBoxLabelFormat(
                        "Your box coordinates and labels should be integers"
                    )
            box_tensor, label_tensor = (
                torch.as_tensor(box, dtype=torch.float32),
                torch.as_tensor(label, dtype=torch.int64),
            )
            imgs.append(img)
            boxes.append(box_tensor)
            labels.append(label_tensor)
    return imgs, boxes, labels

def intersection(a, b):
    '''
        input: 2 boxes (a,b)
        output: overlapping area, if any
    '''
    top = max(a[0], b[0])
    left = max(a[1], b[1])
    bottom = min(a[2], b[2])
    right = min(a[3], b[3])
    h = max(bottom - top, 0)
    w = max(right - left, 0)
    return h * w

def union(a, b):
    a_area = (a[2] - a[0]) * (a[3] - a[1])
    b_area = (b[2] - b[0]) * (b[3] - b[1])
    return a_area + b_area - intersection(a,b)

def c(a, b):
    '''
        input: 2 boxes (a,b)
        output: smallest enclosing bounding box
    '''
    top = min(a[0], b[0])
    left = min(a[1], b[1])
    bottom = max(a[2], b[2])
    right = max(a[3], b[3])
    h = max(bottom - top, 0)
    w = max(right - left, 0)
    return h * w

def iou(a, b):
    '''
        input: 2 boxes (a,b)
        output: Itersection/Union
    '''
    U = union(a,b)
    if U == 0:
        return 0
    return intersection(a,b) / U


def giou(a, b):
    '''
        input: 2 boxes (a,b)
        output: Itersection/Union - (c - U)/c
    '''
    I = intersection(a,b)
    U = union(a,b)
    C = c(a,b)
    iou_term = (I / U) if U > 0 else 0
    giou_term = ((C - U) / C) if C > 0 else 0
    #print("  I: %f, U: %f, C: %f, iou_term: %f, giou_term: %f"%(I,U,C,iou_term,giou_term))
    return iou_term - giou_term

def bb_intersection_over_union(box_a, box_b):
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(box_a[0], box_b[0])
    yA = max(box_a[1], box_b[1])
    xB = min(box_a[2], box_b[2])
    yB = min(box_a[3], box_b[3])
    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (box_a[2] - box_a[0] + 1) * (box_a[3] - box_a[1] + 1)
    boxBArea = (box_b[2] - box_b[0] + 1) * (box_b[3] - box_b[1] + 1)
    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)
    # return the intersection over union value
    return iou

class Evaluator:
    def GetPascalVOCMetrics(
        self, predictions, annotations, IOUThreshold=0.3,
    ):
        """Get the metrics used by the VOC Pascal 2012 challenge.
        Get
        Args:
            boundingboxes: Object of the class BoundingBoxes representing ground truth and detected
            bounding boxes;
            IOUThreshold: IOU threshold indicating which detections will be considered TP or FP
            (default value = 0.5);
            method (default = EveryPointInterpolation): It can be calculated as the implementation
            in the official PASCAL VOC toolkit (EveryPointInterpolation), or applying the 11-point
            interpolatio as described in the paper "The PASCAL Visual Object Classes(VOC) Challenge"
            or EveryPointInterpolation"  (ElevenPointInterpolation);
        Returns:
            A list of dictionaries. Each dictionary contains information and metrics of each class.
            The keys of each dictionary are:
            dict['class']: class representing the current dictionary;
            dict['precision']: array with the precision values;
            dict['recall']: array with the recall values;
            dict['AP']: average precision;
            dict['interpolated precision']: interpolated precision values;
            dict['interpolated recall']: interpolated recall values;
            dict['total positives']: total number of ground truth positives;
            dict['total TP']: total number of True Positive detections;
            dict['total FP']: total number of False Positive detections;
        """
        ret = (
            []
        )  # list containing metrics (precision, recall, average precision) of each class
        # List with all ground truths (Ex: [imageName,class,confidence=1, (bb coordinates XYX2Y2)])
        groundTruths = []
        # List with all detections (Ex: [imageName,class,confidence,(bb coordinates XYX2Y2)])
        detections = []
        # Get all classes
        classes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        for i, dict in enumerate(annotations):
            image_name = i
            for j, bb in enumerate(dict[1]["labels"]):
                class_id = dict[1]["labels"][j]
                box = dict[1]["boxes"][j].numpy()
                groundTruths.append([image_name, class_id, 1, box])
            for j, bb in enumerate(predictions[i][0]["labels"]):
                class_id = predictions[i][0]["labels"][j]
                box = predictions[i][0]["boxes"][j].numpy()
                score = predictions[i][0]["scores"][j]
                detections.append([image_name, class_id, score, box])

        classes = sorted(classes)
        # Precision x Recall is obtained individually by each class
        # Loop through by classes
        for c in classes:
            # Get only detection of class c
            dects = []
            [dects.append(d) for d in detections if d[1] == c]
            # Get only ground truths of class c
            gts = []
            [gts.append(g) for g in groundTruths if g[1] == c]
            npos = len(gts)
            # sort detections by decreasing confidence
            dects = sorted(dects, key=lambda conf: conf[2], reverse=True)
            TP = np.zeros(len(dects))
            FP = np.zeros(len(dects))
            # create dictionary with amount of gts for each image
            det = Counter([cc[0] for cc in gts])
            for key, val in det.items():
                det[key] = np.zeros(val)
            # print("Evaluating class: %s (%d detections)" % (str(c), len(dects)))
            # Loop through detections
            for d in range(len(dects)):
                # print('dect %s => %s' % (dects[d][0], dects[d][3],))
                # Find ground truth image
                gt = [gt for gt in gts if gt[0] == dects[d][0]]
                iouMax = sys.float_info.min
                for j in range(len(gt)):
                    # print('Ground truth gt => %s' % (gt[j][3],))
                    iou = bb_intersection_over_union(dects[d][3], gt[j][3])
                    # iou = Evaluator.iou(dects[d][3], gt[j][3])
                    if iou > iouMax:
                        iouMax = iou
                        jmax = j
                # Assign detection as true positive/don't care/false positive
                if iouMax >= IOUThreshold:
                    if det[dects[d][0]][jmax] == 0:
                        TP[d] = 1  # count as true positive
                        det[dects[d][0]][jmax] = 1  # flag as already 'seen'
                        # print("TP")
                    else:
                        FP[d] = 1  # count as false positive
                        # print("FP")
                # - A detected "cat" is overlaped with a GT "cat" with IOU >= IOUThreshold.
                else:
                    FP[d] = 1  # count as false positive
                    # print("FP")
            # compute precision, recall and average precision
            acc_FP = np.cumsum(FP)
            acc_TP = np.cumsum(TP)
            rec = acc_TP / npos
            prec = np.divide(acc_TP, (acc_FP + acc_TP))

            [ap, mpre, mrec, _] = Evaluator.ElevenPointInterpolatedAP(rec, prec)
            # add class result in the dictionary to be returned
            r = {
                "class": c,
                "precision": prec,
                "recall": rec,
                "AP": ap,
                "interpolated precision": mpre,
                "interpolated recall": mrec,
                "total positives": npos,
                "total TP": np.sum(TP),
                "total FP": np.sum(FP),
            }
            ret.append(r)
        return ret

    def PlotPrecisionRecallCurve(
        self,
        predictions,
        annotations,
        IOUThreshold=0.3,
        showAP=True,
        showInterpolatedPrecision=True,
        savePath="PrecisionRecallCurve",
    ):
        """PlotPrecisionRecallCurve
        Plot the Precision x Recall curve for a given class.
        Args:
            boundingBoxes: Object of the class BoundingBoxes representing ground truth and detected
            bounding boxes;
            IOUThreshold (optional): IOU threshold indicating which detections will be considered
            TP or FP (default value = 0.5);
            method (default = EveryPointInterpolation): It can be calculated as the implementation
            in the official PASCAL VOC toolkit (EveryPointInterpolation), or applying the 11-point
            interpolatio as described in the paper "The PASCAL Visual Object Classes(VOC) Challenge"
            or EveryPointInterpolation"  (ElevenPointInterpolation).
            showAP (optional): if True, the average precision value will be shown in the title of
            the graph (default = False);
            showInterpolatedPrecision (optional): if True, it will show in the plot the interpolated
             precision (default = False);
            savePath (optional): if informed, the plot will be saved as an image in this path
            (ex: /home/mywork/ap.png) (default = None);
            showGraphic (optional): if True, the plot will be shown (default = True)
        Returns:
            A list of dictionaries. Each dictionary contains information and metrics of each class.
            The keys of each dictionary are:
            dict['class']: class representing the current dictionary;
            dict['precision']: array with the precision values;
            dict['recall']: array with the recall values;
            dict['AP']: average precision;
            dict['interpolated precision']: interpolated precision values;
            dict['interpolated recall']: interpolated recall values;
            dict['total positives']: total number of ground truth positives;
            dict['total TP']: total number of True Positive detections;
            dict['total FP']: total number of False Negative detections;
        """

        english_tags = [
            "Homogeneous Liver",
            "Liver with Lesion",
            "Benign Lesion",
            "Malignant Lesion",
            "Cyst",
            "Angioma",
            "NFH",
            "Metastasis",
            "CHC",
            "Adenoma",
        ]
        results = self.GetPascalVOCMetrics(predictions, annotations, IOUThreshold)
        result = None
        # Each resut represents a class
        for result in results:
            classId = result["class"]
            precision = result["precision"]
            recall = result["recall"]
            average_precision = result["AP"]
            mpre = result["interpolated precision"]
            mrec = result["interpolated recall"]
            npos = result["total positives"]
            total_tp = result["total TP"]
            total_fp = result["total FP"]

            plt.close()
            if showInterpolatedPrecision:
                # Uncomment the line below if you want to plot the area
                # plt.plot(mrec, mpre, 'or', label='11-point interpolated precision')
                # Remove duplicates, getting only the highest precision of each recall value
                nrec = []
                nprec = []
                for idx in range(len(mrec)):
                    r = mrec[idx]
                    if r not in nrec:
                        idxEq = np.argwhere(mrec == r)
                        nrec.append(r)
                        nprec.append(max([mpre[int(id)] for id in idxEq]))
                plt.plot(nrec, nprec, "or", label="11-point interpolated precision")
            plt.plot(recall, precision, label="Precision")
            plt.xlabel("recall")
            plt.ylabel("precision")
            if showAP:
                ap_str = "{0:.2f}%".format(average_precision * 100)
                # ap_str = "{0:.4f}%".format(average_precision * 100)
                plt.title(
                    "Precision x Recall curve \nClass: %s, AP: %s"
                    % (str(english_tags[classId - 1]), ap_str)
                )
            else:
                plt.title(
                    "Precision x Recall curve \nClass: %s"
                    % str(english_tags[classId - 1])
                )
            plt.legend(shadow=True)
            plt.grid()
            if savePath is not None:
                plt.savefig(savePath + str(english_tags[classId - 1]) + ".png")
        return results

    @staticmethod
    def CalculateAveragePrecision(rec, prec):
        mrec = []
        mrec.append(0)
        [mrec.append(e) for e in rec]
        mrec.append(1)
        mpre = []
        mpre.append(0)
        [mpre.append(e) for e in prec]
        mpre.append(0)
        for i in range(len(mpre) - 1, 0, -1):
            mpre[i - 1] = max(mpre[i - 1], mpre[i])
        ii = []
        for i in range(len(mrec) - 1):
            if mrec[1:][i] != mrec[0:-1][i]:
                ii.append(i + 1)
        ap = 0
        for i in ii:
            ap = ap + np.sum((mrec[i] - mrec[i - 1]) * mpre[i])
        # return [ap, mpre[1:len(mpre)-1], mrec[1:len(mpre)-1], ii]
        return [ap, mpre[0 : len(mpre) - 1], mrec[0 : len(mpre) - 1], ii]

    @staticmethod
    # 11-point interpolated average precision
    def ElevenPointInterpolatedAP(rec, prec):
        # def CalculateAveragePrecision2(rec, prec):
        mrec = []
        # mrec.append(0)
        [mrec.append(e) for e in rec]
        # mrec.append(1)
        mpre = []
        # mpre.append(0)
        [mpre.append(e) for e in prec]
        # mpre.append(0)
        recallValues = np.linspace(0, 1, 11)
        recallValues = list(recallValues[::-1])
        rhoInterp = []
        recallValid = []
        # For each recallValues (0, 0.1, 0.2, ... , 1)
        for r in recallValues:
            # Obtain all recall values higher or equal than r
            argGreaterRecalls = np.argwhere(mrec[:] >= r)
            pmax = 0
            # If there are recalls above r
            if argGreaterRecalls.size != 0:
                pmax = max(mpre[argGreaterRecalls.min() :])
            recallValid.append(r)
            rhoInterp.append(pmax)
        # By definition AP = sum(max(precision whose recall is above r))/11
        ap = sum(rhoInterp) / 11
        # Generating values for the plot
        rvals = []
        rvals.append(recallValid[0])
        [rvals.append(e) for e in recallValid]
        rvals.append(0)
        pvals = []
        pvals.append(0)
        [pvals.append(e) for e in rhoInterp]
        pvals.append(0)
        # rhoInterp = rhoInterp[::-1]
        cc = []
        for i in range(len(rvals)):
            p = (rvals[i], pvals[i - 1])
            if p not in cc:
                cc.append(p)
            p = (rvals[i], pvals[i])
            if p not in cc:
                cc.append(p)
        recallValues = [i[0] for i in cc]
        rhoInterp = [i[1] for i in cc]
        return [ap, rhoInterp, recallValues, None]

