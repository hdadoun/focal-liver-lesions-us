import argparse

from main import get_args_parser
from models import build_model
from detr_demo.utils import *
torch.set_grad_enabled(False)
from tqdm import tqdm
import PIL  # type: ignore

from PIL import Image  # type: ignore
from torch.utils.data import DataLoader  # type:ignore

from typing import Dict, List, Tuple, Union

import torch  # type: ignore
from typing_extensions import TypedDict

from PIL import Image

import numpy as np


class InvalidFileFormat(Exception):
    pass


class InvalidBoxLabelFormat(Exception):
    pass


class InvalidFileName(Exception):
    pass


def get_transform(train):
    transforms = [T.ToTensor()]
    if train:
        transforms.append(T.RandomHorizontalFlip(0.5))
    return T.Compose(transforms)


class DatasetFasterRCNN(torch.utils.data.dataset.Dataset):

    def __init__(self, root: str, path_to_file: str):
        self.root = root
        self.path_to_file = path_to_file
        self.imgs, self.boxes, self.labels = get_imgs_boxes_labels(
            self.root, self.path_to_file
        )
        self.transform = T.Compose([
            T.Resize(800),
            T.ToTensor(),
            T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])

    def __getitem__(self, idx: int) -> Tuple[torch.Tensor, Dict[str, torch.Tensor]]:
        img = self.imgs[idx]
        boxes = self.boxes[idx]
        labels = self.labels[idx]
        image_id = torch.tensor([idx])
        # suppose all instances are not crowd
        target = {
            "boxes": boxes,
            "labels": labels,
            "image_id": image_id,
            'size': img.size
        }

        img = self.transform(img).unsqueeze(0)

        return img, target

    def __len__(self):
        return len(self.imgs)


def xyxy_to_xywh(xyxy):
    """Convert [x1 y1 x2 y2] box format to [x1 y1 w h] format."""
    if isinstance(xyxy, (list, tuple)):
        # Single box given as a list of coordinates
        assert len(xyxy) == 4
        x1, y1 = xyxy[0], xyxy[1]
        w = xyxy[2] - x1 + 1
        h = xyxy[3] - y1 + 1
        return (x1, y1, w, h)
    elif isinstance(xyxy, np.ndarray):
        # Multiple boxes given as a 2D ndarray
        return np.hstack((xyxy[:, 0:2], xyxy[:, 2:4] - xyxy[:, 0:2] + 1))
    else:
        raise TypeError('Argument xyxy must be a list, tuple, or numpy array.')


def get_imgs_boxes_labels(
        root: str, path_to_file: str
) -> Tuple[List[PIL.Image.Image], List[torch.Tensor], List[torch.Tensor]]:
    with open(path_to_file) as f:
        lines = f.readlines()
        imgs = []
        boxes = []
        labels = []
        for line in sorted(lines):
            splitted = line.strip().split()
            img_path = root + "data_images/" + splitted[0]
            try:
                img = Image.open(img_path).convert("RGB")
                height, width = img.size
            except FileNotFoundError:
                raise InvalidFileName(
                    "We could not find your image, check the image name given"
                )

            num_objs = (len(splitted) - 1) / 5
            if not num_objs.is_integer():
                raise InvalidFileFormat(
                    "The format for detection should be img_name.jpj xmin ymin xmax ymax label"
                )
            box = []
            label = []
            for i in range(int(num_objs)):
                try:
                    xmin = splitted[1 + 5 * i]
                    ymin = splitted[2 + 5 * i]
                    xmax = splitted[3 + 5 * i]
                    ymax = splitted[4 + 5 * i]
                    # x, y, h, w = xyxy_to_xywh([float(xmin), float(ymin), float(xmax), float(ymax)])
                    bounding_box = [float(xmin), float(ymin), float(xmax), float(ymax)]
                    box.append(bounding_box)
                    label.append(int(splitted[5 + 5 * i]))
                except ValueError:
                    raise InvalidBoxLabelFormat(
                        "Your box coordinates and labels should be integers"
                    )
            box_tensor, label_tensor = (
                torch.as_tensor(box, dtype=torch.float32),
                torch.as_tensor(label, dtype=torch.int64),
            )
            imgs.append(img)
            boxes.append(box_tensor)
            labels.append(label_tensor)
    return imgs, boxes, labels
def intersection(a, b):
    '''
        input: 2 boxes (a,b)
        output: overlapping area, if any
    '''
    top = max(a[0], b[0])
    left = max(a[1], b[1])
    bottom = min(a[2], b[2])
    right = min(a[3], b[3])
    h = max(bottom - top, 0)
    w = max(right - left, 0)
    return h * w

def union(a, b):
    a_area = (a[2] - a[0]) * (a[3] - a[1])
    b_area = (b[2] - b[0]) * (b[3] - b[1])
    return a_area + b_area - intersection(a,b)

def c(a, b):
    '''
        input: 2 boxes (a,b)
        output: smallest enclosing bounding box
    '''
    top = min(a[0], b[0])
    left = min(a[1], b[1])
    bottom = max(a[2], b[2])
    right = max(a[3], b[3])
    h = max(bottom - top, 0)
    w = max(right - left, 0)
    return h * w

def iou(a, b):
    '''
        input: 2 boxes (a,b)
        output: Itersection/Union
    '''
    U = union(a,b)
    if U == 0:
        return 0
    return intersection(a,b) / U


def giou(a, b):
    '''
        input: 2 boxes (a,b)
        output: Itersection/Union - (c - U)/c
    '''
    I = intersection(a,b)
    U = union(a,b)
    C = c(a,b)
    iou_term = (I / U) if U > 0 else 0
    giou_term = ((C - U) / C) if C > 0 else 0
    #print("  I: %f, U: %f, C: %f, iou_term: %f, giou_term: %f"%(I,U,C,iou_term,giou_term))
    return iou_term - giou_term

def bb_intersection_over_union(box_a, box_b):
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(box_a[0], box_b[0])
    yA = max(box_a[1], box_b[1])
    xB = min(box_a[2], box_b[2])
    yB = min(box_a[3], box_b[3])
    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (box_a[2] - box_a[0] + 1) * (box_a[3] - box_a[1] + 1)
    boxBArea = (box_b[2] - box_b[0] + 1) * (box_b[3] - box_b[1] + 1)
    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)
    # return the intersection over union value
    return iou


class Prediction(TypedDict):
    boxes: List[torch.Tensor]
    scores: List[torch.Tensor]
    labels: List[float]


def nms_perlabel(
    prediction: List[Dict[str, torch.Tensor]], thresh_score: List[float]
) -> Prediction:
    predictions: Prediction = {"boxes": [], "scores": [], "labels": []}
    boxes = prediction[0]["boxes"]
    scores = prediction[0]["scores"]
    idxs_to_keep = np.arange(len(scores))
    if len(idxs_to_keep) != 0:
        for i in idxs_to_keep:
            idx_score: int = int(prediction[0]["labels"][i].item() - 1)
            if scores[i].item() >= thresh_score[idx_score]:
                predictions["boxes"].append(boxes[i])
                predictions["scores"].append(scores[i])
                predictions["labels"].append(prediction[0]["labels"][i].item())
    return predictions


def evaluate_image_precision(
    final_prediction: Prediction,
    annotation: Dict[str, Union[torch.Tensor]],
    num_classes: int = 19,
    iou_th: float = 0.3,
) -> Tuple[np.ndarray, np.ndarray]:
    len_predictions = np.zeros(num_classes)
    for label in range(num_classes):
        len_predictions[label] = final_prediction["labels"].count(label + 1)
    correct_preds = np.zeros(num_classes)
    for i, label_pred in enumerate(final_prediction["labels"]):
        for j, label_truth in enumerate(annotation["labels"]):
            if label_pred == label_truth.item():
                box_a = final_prediction["boxes"][i].numpy()
                box_b = annotation["boxes"][j].numpy()
                iou = bb_intersection_over_union(box_a, box_b)
                # iou = giou(box_a, box_b)
                if iou >= iou_th:
                    correct_preds[label_pred - 1] += 1
                    break
    return len_predictions, correct_preds


def evaluate_image_recall(
    final_prediction: Prediction,
    annotation: Dict[str, Union[torch.Tensor]],
    num_classes: int = 19,
    iou_th: float = 0.3,
) -> Tuple[np.ndarray, np.ndarray]:

    len_annotation = np.zeros(num_classes)
    for label in range(num_classes):
        len_annotation[label] = len((annotation["labels"] == label + 1).nonzero())
    correct_annotation = np.zeros(num_classes)
    for j, label_truth in enumerate(annotation["labels"]):
        for i, label_prediction in enumerate(final_prediction["labels"]):
            if label_prediction == label_truth.item():
                box_a = final_prediction["boxes"][i].numpy()
                box_b = annotation["boxes"][j].numpy()
                iou = bb_intersection_over_union(box_a, box_b)
                # iou = giou(box_a,box_b)
                if iou >= iou_th:
                    correct_annotation[label_prediction - 1] += 1
                    break
    return len_annotation, correct_annotation


def evaluate_dataset(
    predictions: List[List[Dict[str, torch.Tensor]]],
    annotations: DatasetFasterRCNN,
    thresh_score: List[float],
    num_classes: int = 19,
    iou_th: float = 0.4,
) -> Tuple[List[float], List[float]]:
    all_len_predictions, all_correct_predictions = (
        np.zeros(num_classes),
        np.zeros(num_classes),
    )
    all_len_annotations, all_correct_annotations = (
        np.zeros(num_classes),
        np.zeros(num_classes),
    )
    for i, prediction in enumerate(predictions):
        final_prediction = nms_perlabel(prediction, thresh_score)
        annotation = annotations[i][1]
        len_predictions, correct_predictions = evaluate_image_precision(
            final_prediction, annotation, num_classes, iou_th
        )
        len_annotation, correct_annotation = evaluate_image_recall(
            final_prediction, annotation, num_classes, iou_th
        )
        all_len_predictions += len_predictions
        all_correct_predictions += correct_predictions
        all_len_annotations += len_annotation
        all_correct_annotations += correct_annotation
    precision = all_correct_predictions / (all_len_predictions + 1e-5)
    recall = all_correct_annotations / (all_len_annotations + 1e-5)
    return precision, recall




parser = argparse.ArgumentParser('DETR training and evaluation script', parents=[get_args_parser()])
args = parser.parse_args()
args.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model, criterion, postprocessors = build_model(args)
model.to(args.device)
model.load_state_dict(torch.load('logdir/checkpoint.pth',map_location=args.device)['model'])
dataset = DatasetFasterRCNN(
        root='/user/hdadoun/home/PycharmProjects/GithubProjects/FasterRCNN-v2/Liver/',
        path_to_file='/user/hdadoun/home/PycharmProjects/GithubProjects/FasterRCNN-v2/Liver/data_eval.txt',
    )
model.eval()
predictions =[]
for i in tqdm(range(len(dataset))):
    img, target = dataset[i]
    outputs = model(img)
    probas = outputs['pred_logits'].softmax(-1)[0, :, :-1]
    result = {}
    result['scores'] = torch.max(probas, 1)[0]
    result['labels'] = torch.max(probas, 1)[1] + 1
    result['boxes'] = rescale_bboxes(outputs['pred_boxes'][0], target['size'])
    predictions.append([result])


for prediction in predictions:
    for dicts in prediction:
        for keys in dicts:
            dicts[keys] = dicts[keys].detach().cpu()

# Find thresh
scores = np.arange(0, 1, 0.05)
f1s = []
for i in scores:
    # print("Threshold : ", i, flush=True)
    thresh_score = np.ones(11 - 1) * i
    precision, recall = evaluate_dataset(
        predictions, dataset, thresh_score=thresh_score, num_classes=(args.num_classes),iou_th=0.3
    )
    # print(precision,recall,flush=True)
    f1 = (2 * np.array(precision) * np.array(recall)) / (precision + recall + 1e-5)
    f1s.append(f1)
    print(f1)

print([scores[i] for i in np.array(f1s).argmax(axis=0)])