from typing import Callable, Dict, List, Tuple

import PIL  # type: ignore
import torch  # type: ignore
from bbaug import policies
from bbaug.policies import POLICY_TUPLE as policy
from numpy import asarray
from PIL import Image  # type: ignore
from torch.utils.data import DataLoader  # type:ignore

from lib import transforms as T


class InvalidFileFormat(Exception):
    pass


class InvalidBoxLabelFormat(Exception):
    pass


class InvalidFileName(Exception):
    pass


def get_transform(train):
    transforms = [T.ToTensor()]
    if train:
        transforms.append(T.RandomHorizontalFlip(0.5))
    return T.Compose(transforms)


class DatasetFasterRCNN(torch.utils.data.dataset.Dataset):
    def policy_container(self):
        aug_policy = [
            [
                policy(name="Cutout", probability=0.6, magnitude=8),
                policy(name="Sharpness", probability=0.4, magnitude=8),
            ],
            [
                policy(name="Rotate", probability=0.4, magnitude=8),
                policy(name="Sharpness", probability=0.4, magnitude=2),
                policy(name="Rotate", probability=0.8, magnitude=10),
            ],
            [
                policy(name="Translate_Y", probability=1.0, magnitude=8),
                policy(name="Auto_Contrast", probability=0.8, magnitude=2),
            ],
            [
                policy(name="Auto_Contrast", probability=0.4, magnitude=6),
                policy(name="Shear_X", probability=0.8, magnitude=8),
                policy(name="Brightness", probability=0.0, magnitude=10),
            ],
            [
                policy(name="Solarize_Add", probability=0.2, magnitude=6),
                policy(name="Contrast", probability=0.0, magnitude=10),
                policy(name="Auto_Contrast", probability=0.6, magnitude=0),
            ],
            [
                policy(name="Cutout", probability=0.2, magnitude=0),
            ],
            [
                policy(name="Translate_Y", probability=0.0, magnitude=4),
                policy(name="Equalize", probability=0.6, magnitude=8),
            ],
            [
                policy(name="Translate_Y", probability=0.2, magnitude=2),
                policy(name="Shear_Y", probability=0.8, magnitude=8),
                policy(name="Rotate", probability=0.8, magnitude=8),
            ],
            [
                policy(name="Cutout", probability=0.8, magnitude=8),
                policy(name="Brightness", probability=0.8, magnitude=8),
                policy(name="Cutout", probability=0.2, magnitude=2),
            ],
            [
                policy(name="Color", probability=0.8, magnitude=4),
                policy(name="Translate_Y", probability=1.0, magnitude=6),
                policy(name="Rotate", probability=0.6, magnitude=6),
            ],
            [
                policy(name="Rotate", probability=0.6, magnitude=10),
                policy(name="Cutout_Fraction", probability=1.0, magnitude=4),
                policy(name="Cutout", probability=0.2, magnitude=8),
            ],
            [
                policy(name="Rotate", probability=0.0, magnitude=0),
                policy(name="Equalize", probability=0.6, magnitude=6),
                policy(name="Shear_Y", probability=0.6, magnitude=8),
            ],
            [
                policy(name="Brightness", probability=0.8, magnitude=8),
                policy(name="Auto_Contrast", probability=0.4, magnitude=2),
                policy(name="Brightness", probability=0.2, magnitude=2),
            ],
            [
                policy(name="Translate_Y", probability=0.4, magnitude=8),
            ],
            [
                policy(name="Contrast", probability=1.0, magnitude=10),
                policy(name="Equalize", probability=0.2, magnitude=4),
            ],
        ]
        policy_container = policies.PolicyContainer(aug_policy)
        return policy_container

    def __init__(self, root: str, path_to_file: str, transforms: Callable = None,train: bool =True):
        self.root = root
        self.path_to_file = path_to_file
        self.imgs, self.boxes, self.labels = get_imgs_boxes_labels(
            self.root, self.path_to_file
        )
        self.transforms = transforms
        self.train = train
        self.poc = self.policy_container()


    def __getitem__(self, idx: int) -> Tuple[torch.Tensor, Dict[str, torch.Tensor]]:
        img = self.imgs[idx]
        boxes = self.boxes[idx]
        labels = self.labels[idx]
        num_objs = len(labels)
        image_id = torch.tensor([idx])
        # suppose all instances are not crowd
        iscrowd = torch.zeros((num_objs,), dtype=torch.int64)
        if self.train:
            # Select a random sub-policy from the policy list
            random_policy = self.poc.select_random_policy()
            # Apply this augmentation to the image, returns the augmented image and bounding boxes
            # The boxes must be at a pixel level. e.g. x_min, y_min, x_max, y_max with pixel values
            img_aug, bbs_aug = self.poc.apply_augmentation(
                random_policy,
                asarray(img),
                boxes.numpy(),
                labels.numpy(),
            )
            if bbs_aug.size > 0:
                bbox_aug = [i[1:] for i in bbs_aug]
                boxes = torch.as_tensor(bbox_aug, dtype=torch.float32)
                img = Image.fromarray(img_aug)
            # Only return the augmented image and bounded boxes if there are
            # boxes present after the image augmentation
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        target = {
            "boxes": boxes,
            "labels": labels,
            "image_id": image_id,
            "area": area,
            "iscrowd": iscrowd,
        }

        if self.transforms is not None:
            img, target = self.transforms(img, target)

        return img, target

    def __len__(self):
        return len(self.imgs)


def get_imgs_boxes_labels(
    root: str, path_to_file: str
) -> Tuple[List[PIL.Image.Image], List[torch.Tensor], List[torch.Tensor]]:
    with open(path_to_file) as f:
        lines = f.readlines()
        imgs = []
        boxes = []
        labels = []
        print(sorted(lines))
        for line in sorted(lines):
            splitted = line.strip().split()
            img_path = root + "data_images/" + splitted[0]
            try:
                img = Image.open(img_path).convert("RGB")
            except FileNotFoundError:
                raise InvalidFileName(
                    "We could not find your image, check the image name given"
                )

            num_objs = (len(splitted) - 1) / 5
            if not num_objs.is_integer():
                raise InvalidFileFormat(
                    "The format for detection should be img_name.jpj xmin ymin xmax ymax label"
                )
            box = []
            label = []
            for i in range(int(num_objs)):
                try:
                    xmin = splitted[1 + 5 * i]
                    ymin = splitted[2 + 5 * i]
                    xmax = splitted[3 + 5 * i]
                    ymax = splitted[4 + 5 * i]
                    box.append([float(xmin), float(ymin), float(xmax), float(ymax)])
                    label.append(int(splitted[5 + 5 * i]))
                except ValueError:
                    raise InvalidBoxLabelFormat(
                        "Your box coordinates and labels should be integers"
                    )
            box_tensor, label_tensor = (
                torch.as_tensor(box, dtype=torch.float32),
                torch.as_tensor(label, dtype=torch.int64),
            )
            imgs.append(img)
            boxes.append(box_tensor)
            labels.append(label_tensor)
    return imgs, boxes, labels
