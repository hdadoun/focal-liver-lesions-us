from typing import List

from typing_extensions import TypedDict


class Config(TypedDict):
    task: str
    root: str
    path_to_train: str
    path_to_eval: str
    model_name: str
    num_classes: int
    num_epochs: int
    batch_size_train: int
    lr: float
    momentum: float
    weight_decay: float
    train: bool
    weight_name: str
    iou_thresh: List[float]
    milestones: List[int]


##Detect Liver
# config: Config = {
#     "task": "detection",
#     "root": "Liver/",
#     "model_name": "FasterRCNN",
#     "num_classes": 3,
#     "num_epochs": 36,
#     "batch_size_train": 4,
#     "lr": 0.005,
#     "momentum": 0.9,
#     "weight_decay": 0.0005,
#     "train": False,
#     "weight_name": "model_FasterRCNN_detection_Liver_epoch_35.pth",
#     "iou_thresh": [0.5, 0.6],
# }

# Detect Liver and Lesion
# config = {
#     "task": "detection",
#     "root": "Liver/",
#     "model_name": "FasterRCNN",
#     "num_classes": 4,
#     "num_epochs": 36,
#     "batch_size_train": 4,
#     "lr": 0.005,
#     "momentum": 0.9,
#     "weight_decay": 0.0005,
#     "train": False,
#     "weight_name": "model_FasterRCNN_detection_LiverLesion_epoch_35.pth",
#     "iou_thresh": [0.5, 0.6,0.5],
# }
#

##Detect Organs
# config = {
#     "task": "detection",
#     "root": "Organs/",
#     "path_to_train": "data_train.txt",
#     "path_to_eval": "data_eval.txt",
#     "model_name": "FasterRCNN",
#     "num_classes": 10,
#     "num_epochs": 100,
#     "batch_size_train": 2,
#     "lr": 0.0048,
#     "momentum": 0.9,
#     "weight_decay": 0.0001,
#     "train":False,
#     "weight_name": "model_FasterRCNN_detection_organs_aLRP_epoch_100.pth",
#     "iou_thresh": [0.75, 0.65, 0.65, 0.75, 0.9, 0.8, 0.65, 0.7, 0.55],
#     'milestones': [75,90,140]
# }


## Detect Organs and FLLs
# config = {
#     "task": "detection",
#     "root": "Whole-Dataset/",
#     "path_to_train": "data_train.txt",
#     "path_to_eval": "data_eval.txt",
#     "model_name": "FasterRCNN",
#     "num_classes": 18,
#     "num_epochs": 100,
#     "batch_size_train": 2,
#     "lr": 0.0048,
#     "momentum": 0.9,
#     "weight_decay": 0.0001,
#     "train":False,
#     "weight_name": "model_FasterRCNN_detection_Organs_FLLs_aLRP_epoch_100.pth",
#     "iou_thresh": [0.9, 0.75, 0.65, 0.75, 0.65, 0.75, 0.75, 0.70, 0.70, 0.65, 0.70, 0.9, 0.75, 0.95, 0.9, 0.85, 0.85],
#     'milestones': [75,90,140]
# }
#Detect Liver and LesionSS
config = {
    "task": "detection",
    "root": "FLL-Test/",
    "path_to_train": "data_train.txt",
    "path_to_eval": "data_eval.txt",
    "model_name": "FasterRCNN",
    "num_classes": 11,
    "num_epochs": 100,
    "batch_size_train": 4,
    "lr": 0.0048,
    "momentum": 0.9,
    "weight_decay": 0.0001,
    "train":False,
    "weight_name": "model_FasterRCNN_detection_LiverLesionSS_aLRP_NEWSET_epoch_50.pth",
    "iou_thresh": [0.65, 0.65, 0.50, 0.35, 0.40, 0.55, 0.35, 0.40, 0.25, 0.15],
    'milestones': [75,90,140]
}
# [0.85, 0.85, 0.70, 0.55, 0.60, 0.75, 0.55, 0.60, 0.45, 0.35]
# config = {
#     "task": "detection",
#     "root": "Liver/",
#     "path_to_train": "data_train.txt",
#     "path_to_eval": "data_eval.txt",
#     "model_name": "FasterRCNN",
#     "num_classes": 11,
#     "num_epochs": 50,
#     "batch_size_train": 4,
#     "lr": 0.0048,
#     "momentum": 0.9,
#     "weight_decay": 0.0001,
#     "train":True,
#     "weight_name": "model_FasterRCNN_detection_LiverLesionSS_aLRP_NEWSET_epoch_50.pth",
#     "iou_thresh": [0.8, 0.9, 0.3, 0.3, 0.2, 0.5, 0.1, 0.2, 0.3, 0.3],
#     'milestones': [20,40,45]
# }

# config = {
#     "task": "detection",
#     "root": "",
#     "path_to_train": "data_train.txt",
#     "path_to_eval": "data_hind.txt",
#     "model_name": "FasterRCNN",
#     "num_classes": 11,
#     "num_epochs": 50,
#     "batch_size_train": 4,
#     "lr": 0.0048,
#     "momentum": 0.9,
#     "weight_decay": 0.0001,
#     "train":False,
#     "weight_name": "model_FasterRCNN_detection_LiverLesionSS_aLRP_epoch_100.pth",
#     "iou_thresh": [0.9, 0.9, 0.69, 0.69, 0.69, 0.69, 0.69, 0.69, 0.69, 0.69],
#     'milestones': [35,45]
# }
# Detect liver type (lesions)
# config = {
#     "task": "detection",
#     "root": "Liver/",
#     "model_name": "FasterRCNN",
#     "num_classes": 8,
#     "num_epochs": 36,
#     "batch_size_train": 4,
#     "lr": 0.005,
#     "momentum": 0.9,
#     "weight_decay": 0.0005,
#     "train": False,
#     "weight_name": "model_FasterRCNN_detection_LiverTypeSS_epoch_35.pth",
#     "iou_thresh": [0.8, 0.9, 0.3, 0.3, 0.2, 0.5, 0.1],
# }

# # ##Detect Lesion
# config = {
#     "task": "detection",
#     "root": "Detect Lesion/",
#     "model_name": "FasterRCNN",
#     "num_classes": 3,
#     "num_epochs": 36,
#     'batch_size_train':2,
#     "lr": 0.003,
#     "momentum": 0.9,
#     "weight_decay": 0.0005,
#     "train": False,
#     "weight_name": "model_FasterRCNN_detection_Lesion_epoch_70.pth",
#     "iou_thresh" : [0.8,0.6]
# }
