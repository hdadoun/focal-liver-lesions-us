import os
import sys

sys.path.append("/home/hdadoun/FasterRCNN-v2")
sys.path.append("//data/epione/share/epione-nhance/epione/")


from glob import glob
from typing import Callable, Dict, List, Tuple

import numpy as np  # type: ignore
import torch
from PIL import Image
import PIL
import re

from lib.evaluator import nms_perlabel
from lib.trainer import get_instance_segmentation_model
from lib import transforms as T
from lib.dataset import InvalidFileName, InvalidFileFormat, InvalidBoxLabelFormat


torch.manual_seed(0)


def write_prediction_file(config, paths_to_all_images, file_name):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    num_classes = config["num_classes"]
    model = get_instance_segmentation_model(num_classes)
    model.load_state_dict(
        torch.load("weights/" + config["weight_name"], map_location=device)
    )
    model.to(device)
    # Get predictions
    predictions = []
    model.eval()
    transforms = T.ToTensor()
    with torch.no_grad():
        for image_path in paths_to_all_images:
            list_ = []
            img = Image.open(image_path).convert("RGB")
            img_name = os.path.basename(image_path)
            img, _ = transforms(img, None)
            prediction = model([img.to(device)])
            for dicts in prediction:
                for keys in dicts:
                    dicts[keys] = dicts[keys].detach().cpu()
            final_prediction = nms_perlabel(prediction, config["iou_thresh"])
            for keys in final_prediction:
                dicts[keys] = dicts[keys].detach().cpu().numpy()
            num_objs = len(final_prediction["labels"])
            for i in range(num_objs):
                xmin = int(final_prediction["boxes"][i][0])
                ymin = int(final_prediction["boxes"][i][1])
                xmax = int(final_prediction["boxes"][i][2])
                ymax = int(final_prediction["boxes"][i][3])
                list_.extend([str(xmin), str(ymin), str(xmax), str(ymax)])
                list_.append(str(final_prediction["labels"][i]))
            if len(list_) != 0:
                list_.insert(0, str(img_name))
                list_.append("\n")
                predictions.append(list_)

    sep = " "
    s = ""
    list_text = s.join([sep.join(i) for i in predictions])
    file1 = open(file_name, "w")
    file1.writelines(list_text)
    file1.close()
    return


def find_label_and_write_prediction(
    file_name="predictions_organs.txt", label=[4,5], new_file_name="predictions_lesions.txt"
):
    names, boxes, labels = get_imgs_boxes_labels(
        "//data/epione/share/epione-nhance/epione/HIND-DADOUN/Processed/", file_name
    )

    predictions = []
    for idx in range(len(names)):
        list_ = []
        if 1 in labels[idx]:
            for i in range(len(labels[idx])):
                if labels[idx][i].item() in label:
                    xmin = int(boxes[idx][i][0])
                    ymin = int(boxes[idx][i][1])
                    xmax = int(boxes[idx][i][2])
                    ymax = int(boxes[idx][i][3])
                    list_.extend([str(xmin), str(ymin), str(xmax), str(ymax)])
                    list_.append(str(labels[idx][i].item()))
        if len(list_)!=0:
                list_.insert(0,str(names[idx]))
                list_.append("\n")
                predictions.append(list_)
    sep = " "
    s = ""
    list_text = s.join([sep.join(i) for i in predictions])
    file1 = open(new_file_name, "w")
    file1.writelines(list_text)
    file1.close()
    return

def find_label(
    file_name="predictions_organs.txt", label=1, new_file_name="predictions_liver.txt"
):
    names, boxes, labels = get_imgs_boxes_labels(
        "//data/epione/share/epione-nhance/epione/HIND-DADOUN/Processed/", file_name
    )

    predictions = []
    for idx in range(len(names)):
        list_ = []
        if label in labels[idx]:
        # if label in labels[idx] and len(labels[idx])==1:
            list_.append(str(names[idx]))
            list_.append("\n")
            predictions.append(list_)

    sep = " "
    s = ""
    list_text = s.join([sep.join(i) for i in predictions])
    file1 = open(new_file_name, "w")
    file1.writelines(list_text)
    file1.close()
    return

def get_imgs_boxes_labels(
    root: str, path_to_file: str
) -> Tuple[List[str], List[torch.Tensor], List[torch.Tensor]]:
    with open(path_to_file) as f:
        lines = f.readlines()
        boxes = []
        labels = []
        names = []
        for line in sorted(lines):
            splitted = line.strip().split()
            names.append(splitted[0])

            num_objs = (len(splitted) - 1) / 5
            if not num_objs.is_integer():
                raise InvalidFileFormat(
                    "The format for detection should be img_name.jpj xmin ymin xmax ymax label"
                )
            box = []
            label = []
            for i in range(int(num_objs)):
                try:
                    xmin = splitted[1 + 5 * i]
                    ymin = splitted[2 + 5 * i]
                    xmax = splitted[3 + 5 * i]
                    ymax = splitted[4 + 5 * i]
                    box.append([float(xmin), float(ymin), float(xmax), float(ymax)])
                    label.append(int(splitted[5 + 5 * i]))
                except ValueError:
                    raise InvalidBoxLabelFormat(
                        "Your box coordinates and labels should be integers"
                    )
            box_tensor, label_tensor = (
                torch.as_tensor(box, dtype=torch.float32),
                torch.as_tensor(label, dtype=torch.int64),
            )
            boxes.append(box_tensor)
            labels.append(label_tensor)
    return names, boxes, labels


def stringSplitByNumbers(x):
    r = re.compile("(\d+)")
    l = r.split(x)
    return [int(y) if y.isdigit() else y for y in l]

config = {
    "task": "detection",
    "root": "Whole-Dataset/",
    "path_to_train": "data_train.txt",
    "path_to_eval": "data_eval.txt",
    "model_name": "FasterRCNN",
    "num_classes": 18,
    "num_epochs": 100,
    "batch_size_train": 2,
    "lr": 0.0048,
    "momentum": 0.9,
    "weight_decay": 0.0001,
    "train":False,
    "weight_name": "model_FasterRCNN_detection_Organs_FLLs_aLRP_epoch_100.pth",
    "iou_thresh": [0.9, 0.75, 0.65, 0.75, 0.65, 0.75, 0.75, 0.70, 0.70, 0.65, 0.70, 0.9, 0.75, 0.95, 0.9, 0.85, 0.85],
    'milestones': [75,90,140]
}


paths_to_all_images = sorted(
    glob("//data/epione/share/epione-nhance/epione/HIND-DADOUN/Processed" + "/*.png"),
    key=stringSplitByNumbers,
)
print(len(paths_to_all_images), flush=True)

write_prediction_file(config, paths_to_all_images, "predictions_organs.txt")
find_label_and_write_prediction()


print('Starting',flush=True)
find_label()
print('Finished writing liver file',flush=True)
path_to_file = "predictions_liver.txt"

paths_to_all_images = []
with open(path_to_file) as f:
    lines = f.readlines()
    for line in lines:
        splitted = line.strip().split()
        paths_to_all_images.append(
            "//data/epione/share/epione-nhance/epione/HIND-DADOUN/Processed/"
            + splitted[0]
        )
paths_to_all_images = sorted(paths_to_all_images, key=stringSplitByNumbers,)

config = {
    "task": "detection",
    "root": "Liver/",
    "path_to_train": "data_train.txt",
    "path_to_eval": "data_eval.txt",
    "model_name": "FasterRCNN",
    "num_classes": 11,
    "num_epochs": 100,
    "batch_size_train": 4,
    "lr": 0.0048,
    "momentum": 0.9,
    "weight_decay": 0.0001,
    "train": False,
    "weight_name": "model_FasterRCNN_detection_LiverLesionSS_aLRP_epoch_100.pth",
    "iou_thresh": [0.9, 0.75, 0.60, 0.65, 0.60, 0.70, 0.60, 0.70, 0.60, 0.45],
    "milestones": [75, 90, 140],
}

print(len(paths_to_all_images), flush=True)

print('Started prediction file',flush=True)
write_prediction_file(config, paths_to_all_images, "predictions_flls.txt")
