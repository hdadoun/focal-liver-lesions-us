import json
import os
import re
import sys
from glob import glob

from PIL import Image

from lib import transforms as T

import numpy as np  # type: ignore
import torch
from torch.utils.data import DataLoader

from lib.config import config
from lib.dataset import DatasetFasterRCNN, get_transform
from lib.evaluator import EvaluateFLL, evaluate_dataset,Evaluator,get_accuracy_well_localized,nms_perlabel
from lib.trainer import get_instance_segmentation_model, train_one_epoch
from lib.vision_utils import collate_fn  # type: ignore



torch.manual_seed(0)

def stringSplitByNumbers(x):
    r = re.compile("(\d+)")
    l = r.split(x)
    return [int(y) if y.isdigit() else y for y in l]
if config["train"]:
    root = config["root"]
    path_to_file = config["root"] + config["path_to_train"]
    dataset = DatasetFasterRCNN(
        root=root,
        path_to_file=path_to_file,
        transforms=get_transform(train=config["train"]),
    )
    data_loader = DataLoader(
        dataset,
        batch_size=config["batch_size_train"],
        shuffle=config["train"],
        num_workers=4,
        collate_fn=collate_fn,
    )

    # Initialize model
    num_classes = config["num_classes"]
    model = get_instance_segmentation_model(num_classes)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model.to(device)
    params = [p for p in model.parameters() if p.requires_grad]
    optimizer = torch.optim.SGD(
        params,
        lr=config["lr"],
        momentum=config["momentum"],
        weight_decay=config["weight_decay"],
    )
    # lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.5)
    lr_scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=config['milestones'], gamma=0.9)

    print("Started training with device :", device)

    # Training
    num_epochs = config["num_epochs"]
    for epoch in range(num_epochs):
        # train for one epoch, printing every 10 iterations
        train_one_epoch(model, optimizer, data_loader, device, epoch, print_freq=100)
        # update the learning rate
        lr_scheduler.step()
        torch.save(model.state_dict(), "weights/" + config["weight_name"])

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Prepare dataset eval

root = config["root"]
path_to_file = config["root"] + config["path_to_eval"]

dataset = DatasetFasterRCNN(
    root=root, path_to_file=path_to_file, transforms=get_transform(train=False),train = False
)
print(len(dataset),flush=True)
data_loader = DataLoader(
    dataset, batch_size=1, shuffle=False, num_workers=4, collate_fn=collate_fn,
)

def write_prediction_file(config, paths_to_all_images, file_name):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    num_classes = config["num_classes"]
    model = get_instance_segmentation_model(num_classes)
    model.load_state_dict(
        torch.load("weights/" + config["weight_name"], map_location=device)
    )
    model.to(device)
    # Get predictions
    predictions = []
    model.eval()
    transforms = T.ToTensor()
    with torch.no_grad():
        for image_path in paths_to_all_images:
            list_ = []
            img = Image.open(image_path).convert("RGB")
            img_name = os.path.basename(image_path)
            img, _ = transforms(img, None)
            prediction = model([img.to(device)])
            for dicts in prediction:
                for keys in dicts:
                    dicts[keys] = dicts[keys].detach().cpu()
            final_prediction = nms_perlabel(prediction, config["iou_thresh"])
            for keys in final_prediction:
                dicts[keys] = dicts[keys].detach().cpu().numpy()
            num_objs = len(final_prediction["labels"])
            for i in range(num_objs):
                xmin = int(final_prediction["boxes"][i][0])
                ymin = int(final_prediction["boxes"][i][1])
                xmax = int(final_prediction["boxes"][i][2])
                ymax = int(final_prediction["boxes"][i][3])
                list_.extend([str(xmin), str(ymin), str(xmax), str(ymax)])
                list_.append(str(final_prediction["labels"][i]))
            if len(list_) != 0:
                list_.insert(0, str(img_name))
                list_.append("\n")
                print(list_,flush=True)
                predictions.append(list_)

    sep = " "
    s = ""
    list_text = s.join([sep.join(i) for i in predictions])
    file1 = open(file_name, "w")
    file1.writelines(list_text)
    file1.close()
    return


# Load model
num_classes = config["num_classes"]
model = get_instance_segmentation_model(num_classes)
model.load_state_dict(
    torch.load("weights/" + config["weight_name"], map_location=device)
)
model.to(device)
# Get predictions
predictions = []
model.eval()

with torch.no_grad():
    for i in range(len(dataset)):
        img, _ = dataset[i]
        predictions.append(model([img.to(device)]))

for prediction in predictions:
    for dicts in prediction:
        for keys in dicts:
            dicts[keys] = dicts[keys].detach().cpu()



# Find thresh
scores = np.arange(0, 1, 0.05)
f1s = []
for i in scores:
    thresh_score = np.ones(num_classes - 1) * i

    precision, recall = evaluate_dataset(
        predictions, dataset, thresh_score=thresh_score, num_classes=(num_classes - 1),iou_th=0.3
    )
    f1 = (2 * np.array(precision) * np.array(recall)) / (precision + recall + 1e-5)
    f1s.append(f1)

config["iou_thresh"] = [scores[i] for i in np.array(f1s).argmax(axis=0)]
print('THRESH', config['iou_thresh'],flush=True)

confusion_matrix = get_accuracy_well_localized(predictions,dataset,thresh_score=config["iou_thresh"],iou_th=0.3)
print(confusion_matrix,confusion_matrix.shape, flush=True)
evaluator_ = EvaluateFLL(predictions,dataset,thresh_score=config["iou_thresh"],num_classes=(num_classes - 1),iou_th=0.3)
evaluator_.evaluate_network()
evaluator_ = Evaluator()

evaluator_.PlotPrecisionRecallCurve(predictions,dataset)


precision, recall = evaluate_dataset(
    predictions,
    dataset,
    thresh_score=config["iou_thresh"],
    num_classes=(num_classes - 1),
    iou_th=0.3
)

print("Results per label ")
print("Precision", precision, "Recall", recall, flush=True)
print((2 * np.array(precision) * np.array(recall)) / (precision + recall + 1e-6))

paths_to_all_images = sorted(
    glob("FLL-Test/data_images/*.jpg"),
    key=stringSplitByNumbers,
)
write_prediction_file(config, paths_to_all_images, 'prediction_test_set.txt')

for pred in predictions:
    for dicts in pred:
        for keys in dicts:
            dicts[keys] = dicts[keys].detach().cpu().numpy().tolist()
filename = './predictions_fasterrcnn.json'

with open(filename, 'w') as fd:
    fd.write(json.dumps(predictions))