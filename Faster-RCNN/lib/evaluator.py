from typing import Dict, List, Tuple, Union

import numpy as np  # type: ignore
import torch  # type: ignore
from matplotlib import pyplot as plt  # type: ignore
from typing_extensions import TypedDict

from lib.dataset import DatasetFasterRCNN
from lib.vision_utils import bb_intersection_over_union, giou  # type: ignore
from PIL import Image
import os
import sys
from collections import Counter
import matplotlib.pyplot as plt
import numpy as np
import cv2
from torchvision import transforms
from numpy import asarray
from sklearn.metrics import confusion_matrix,classification_report


class Prediction(TypedDict):
    boxes: List[torch.Tensor]
    scores: List[torch.Tensor]
    labels: List[float]


def nms_perlabel(
    prediction: List[Dict[str, torch.Tensor]], thresh_score: List[float]
) -> Prediction:
    predictions: Prediction = {"boxes": [], "scores": [], "labels": []}
    boxes = prediction[0]["boxes"]
    scores = prediction[0]["scores"]
    idxs_to_keep = np.arange(len(scores))
    if len(idxs_to_keep) != 0:
        for i in idxs_to_keep:
            idx_score: int = int(prediction[0]["labels"][i].item() - 1)
            if scores[i].item() >= thresh_score[idx_score]:
                predictions["boxes"].append(boxes[i])
                predictions["scores"].append(scores[i])
                predictions["labels"].append(prediction[0]["labels"][i].item())
    return predictions


def evaluate_image_precision(
    final_prediction: Prediction,
    annotation: Dict[str, Union[torch.Tensor]],
    num_classes: int = 19,
    iou_th: float = 0.3,
) -> Tuple[np.ndarray, np.ndarray]:
    len_predictions = np.zeros(num_classes)
    for label in range(num_classes):
        len_predictions[label] = final_prediction["labels"].count(label + 1)
    correct_preds = np.zeros(num_classes)
    for i, label_pred in enumerate(final_prediction["labels"]):
        for j, label_truth in enumerate(annotation["labels"]):
            if label_pred == label_truth.item():
                box_a = final_prediction["boxes"][i].numpy()
                box_b = annotation["boxes"][j].numpy()
                iou = bb_intersection_over_union(box_a, box_b)
                # iou = giou(box_a, box_b)
                if iou >= iou_th:
                    correct_preds[label_pred - 1] += 1
                    break
    return len_predictions, correct_preds


def evaluate_image_recall(
    final_prediction: Prediction,
    annotation: Dict[str, Union[torch.Tensor]],
    num_classes: int = 19,
    iou_th: float = 0.3,
) -> Tuple[np.ndarray, np.ndarray]:

    len_annotation = np.zeros(num_classes)
    for label in range(num_classes):
        len_annotation[label] = len((annotation["labels"] == label + 1).nonzero())
    correct_annotation = np.zeros(num_classes)
    for j, label_truth in enumerate(annotation["labels"]):
        for i, label_prediction in enumerate(final_prediction["labels"]):
            if label_prediction == label_truth.item():
                box_a = final_prediction["boxes"][i].numpy()
                box_b = annotation["boxes"][j].numpy()
                iou = bb_intersection_over_union(box_a, box_b)
                # iou = giou(box_a,box_b)
                if iou >= iou_th:
                    correct_annotation[label_prediction - 1] += 1
                    break
    return len_annotation, correct_annotation


def evaluate_dataset(
    predictions: List[List[Dict[str, torch.Tensor]]],
    annotations: DatasetFasterRCNN,
    thresh_score: List[float],
    num_classes: int = 19,
    iou_th: float = 0.4,
) -> Tuple[List[float], List[float]]:
    all_len_predictions, all_correct_predictions = (
        np.zeros(num_classes),
        np.zeros(num_classes),
    )
    all_len_annotations, all_correct_annotations = (
        np.zeros(num_classes),
        np.zeros(num_classes),
    )
    for i, prediction in enumerate(predictions):
        final_prediction = nms_perlabel(prediction, thresh_score)
        annotation = annotations[i][1]
        len_predictions, correct_predictions = evaluate_image_precision(
            final_prediction, annotation, num_classes, iou_th
        )
        len_annotation, correct_annotation = evaluate_image_recall(
            final_prediction, annotation, num_classes, iou_th
        )
        all_len_predictions += len_predictions
        all_correct_predictions += correct_predictions
        all_len_annotations += len_annotation
        all_correct_annotations += correct_annotation
    precision = all_correct_predictions / (all_len_predictions + 1e-5)
    recall = all_correct_annotations / (all_len_annotations + 1e-5)
    return precision, recall


def create_image(image, annotation, color=(0, 255, 0)):
    labels_to_string = {
        1: "Homogeneous Liver",
        2: "Liver with Lesion",
        3: "Benign Lesion",
        4: "Malignant Lesion",
        5: "Cyst",
        6: "Angioma",
        7: "NFH",
        8: "Metastasis",
        9: "CHC",
        10: "Adenoma",
    }
    for idx, boxe in enumerate(annotation["boxes"]):
        xmin, ymin, xmax, ymax = boxe
        cv2.rectangle(image, (int(xmin), int(ymin)), (int(xmax), int(ymax)), color, 2)
        if isinstance(annotation["labels"][idx], int):
            text = labels_to_string[annotation["labels"][idx]]
        else:
            text = labels_to_string[annotation["labels"][idx].item()]

        if text not in ["2B_Lesion_MALIGNE", "2A_Lesion_BENIGNE"]:
            cv2.putText(
                image,
                str(text),
                (int(xmin), int(ymin) - 10),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.9,
                color,
                2,
            )
    return image


class EvaluateFLL:
    def __init__(
        self,
        predictions: List[List[Dict[str, torch.Tensor]]],
        annotations: DatasetFasterRCNN,
        thresh_score: List[float],
        num_classes: int = 11,
        iou_th: float = 0.4,
    ):
        self.predictions = predictions
        self.annotations = annotations
        self.th_score = thresh_score
        self.num_classes = num_classes
        self.th_iou = iou_th

    def get_localization_score(
        self, prediction: Prediction, truth: Dict[str, Union[torch.Tensor]]
    ):
        prediction["labels"] = [i if i in [1, 2] else 3 for i in prediction["labels"]]
        truth["labels"] = torch.as_tensor(
            [i if i in [1, 2] else torch.tensor(3) for i in truth["labels"]]
        )
        len_predictions, correct_predictions = evaluate_image_precision(
            prediction, truth, self.num_classes, self.th_iou
        )
        len_annotation, correct_annotation = evaluate_image_recall(
            prediction, truth, self.num_classes, self.th_iou
        )
        return (
            len_predictions[2],
            correct_predictions[2],
            len_annotation[2],
            correct_annotation[2],
        )

    def classify_liver(self, prediction: Prediction):
        new_dict = {"boxes": [], "labels": [], "scores": []}
        if len(prediction["scores"]) > 0:
            highest_score_idx = prediction["scores"].index(max(prediction["scores"]))
            label = prediction["labels"][highest_score_idx]
            if label == 1:
                new_dict["boxes"].append(prediction["boxes"][highest_score_idx])
                new_dict["labels"].append(1)
                new_dict["scores"].append(prediction["scores"][highest_score_idx])
                return new_dict, True
            else:
                return prediction, False
        else:
            return prediction, True

    def evaluate_network(self,):
        len_pred_locs, correct_pred_locs, len_anno_locs, correct_anno_locs = 0, 0, 0, 0
        accuracy_classify_liver = 0
        all_len_predictions, all_correct_predictions = (
            np.zeros(self.num_classes),
            np.zeros(self.num_classes),
        )
        all_len_annotations, all_correct_annotations = (
            np.zeros(self.num_classes),
            np.zeros(self.num_classes),
        )
        for i, prediction in enumerate(self.predictions):
            final_prediction = nms_perlabel(prediction, self.th_score)
            annotation = self.annotations[i][1]
            final_prediction, is_healthy = self.classify_liver(final_prediction)
            len_predictions, correct_predictions = evaluate_image_precision(
                final_prediction, annotation, self.num_classes, self.th_iou
            )
            len_annotation, correct_annotation = evaluate_image_recall(
                final_prediction, annotation, self.num_classes, self.th_iou
            )
            all_len_predictions += len_predictions
            all_correct_predictions += correct_predictions
            all_len_annotations += len_annotation
            all_correct_annotations += correct_annotation
            if not is_healthy:
                (
                    len_pred_loc,
                    correct_pred_loc,
                    len_anno_loc,
                    correct_anno_loc,
                ) = self.get_localization_score(final_prediction, annotation)
                len_pred_locs += len_pred_loc
                correct_pred_locs += correct_pred_loc
                len_anno_locs += len_anno_loc
                correct_anno_locs += correct_anno_loc
                if 1 not in annotation["labels"]:
                    accuracy_classify_liver += 1
                else:
                    # img = self.annotations[i][0].numpy().transpose(1, 2, 0)
                    img = transforms.ToPILImage()(self.annotations[i][0]).convert("RGB")
                    new_img = create_image(asarray(img), final_prediction)
                    new_img = create_image(
                        asarray(new_img), self.annotations[i][1], color=(255, 0, 255)
                    )
                    cv2.imwrite("results/false_lesion_%d.jpg" % i, new_img)
            else:
                if 1 in annotation["labels"]:
                    accuracy_classify_liver += 1
                else:
                    # img = self.annotations[i][0].numpy().transpose(1, 2, 0)
                    img = transforms.ToPILImage()(self.annotations[i][0]).convert("RGB")
                    new_img = create_image(asarray(img), final_prediction)
                    new_img = create_image(
                        asarray(new_img), self.annotations[i][1], color=(255, 0, 255)
                    )

                    # plt.imsave("results/false_homogeneous_%d.jpg" % i, img)
                    cv2.imwrite("results/false_homogeneous_%d.jpg" % i, new_img)

        accuracy_classify_liver /= len(self.predictions)
        precision = all_correct_predictions / (all_len_predictions + 1e-5)
        recall = all_correct_annotations / (all_len_annotations + 1e-5)
        precision_loc = correct_pred_locs / (len_pred_locs + 1e-5)
        recall_loc = correct_anno_locs / (len_anno_locs + 1e-5)
        print("Accuracy classifier", accuracy_classify_liver, flush=True)
        print("Precision Labels", precision, flush=True)
        print("Recall Labels", recall, flush=True)
        print(
            "F1-Score Labels",
            (2 * np.array(precision) * np.array(recall)) / (precision + recall + 1e-6),
            flush=True,
        )
        print("Precision Localisation", precision_loc, flush=True)
        print("Recall Localisation", recall_loc, flush=True)
        print(
            "F1-Score Localisation",
            (2 * np.array(precision_loc) * np.array(recall_loc))
            / (precision_loc + recall_loc + 1e-6),
            flush=True,
        )
        return


class Evaluator:
    def GetPascalVOCMetrics(
        self, predictions, annotations, IOUThreshold=0.3,
    ):
        """Get the metrics used by the VOC Pascal 2012 challenge.
        Get
        Args:
            boundingboxes: Object of the class BoundingBoxes representing ground truth and detected
            bounding boxes;
            IOUThreshold: IOU threshold indicating which detections will be considered TP or FP
            (default value = 0.5);
            method (default = EveryPointInterpolation): It can be calculated as the implementation
            in the official PASCAL VOC toolkit (EveryPointInterpolation), or applying the 11-point
            interpolatio as described in the paper "The PASCAL Visual Object Classes(VOC) Challenge"
            or EveryPointInterpolation"  (ElevenPointInterpolation);
        Returns:
            A list of dictionaries. Each dictionary contains information and metrics of each class.
            The keys of each dictionary are:
            dict['class']: class representing the current dictionary;
            dict['precision']: array with the precision values;
            dict['recall']: array with the recall values;
            dict['AP']: average precision;
            dict['interpolated precision']: interpolated precision values;
            dict['interpolated recall']: interpolated recall values;
            dict['total positives']: total number of ground truth positives;
            dict['total TP']: total number of True Positive detections;
            dict['total FP']: total number of False Positive detections;
        """
        ret = (
            []
        )  # list containing metrics (precision, recall, average precision) of each class
        # List with all ground truths (Ex: [imageName,class,confidence=1, (bb coordinates XYX2Y2)])
        groundTruths = []
        # List with all detections (Ex: [imageName,class,confidence,(bb coordinates XYX2Y2)])
        detections = []
        # Get all classes
        classes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        for i, dict in enumerate(annotations):
            image_name = i
            for j, bb in enumerate(dict[1]["labels"]):
                class_id = dict[1]["labels"][j]
                box = dict[1]["boxes"][j].numpy()
                groundTruths.append([image_name, class_id, 1, box])
            for j, bb in enumerate(predictions[i][0]["labels"]):
                class_id = predictions[i][0]["labels"][j]
                box = predictions[i][0]["boxes"][j].numpy()
                score = predictions[i][0]["scores"][j]
                detections.append([image_name, class_id, score, box])

        classes = sorted(classes)
        # Precision x Recall is obtained individually by each class
        # Loop through by classes
        for c in classes:
            # Get only detection of class c
            dects = []
            [dects.append(d) for d in detections if d[1] == c]
            # Get only ground truths of class c
            gts = []
            [gts.append(g) for g in groundTruths if g[1] == c]
            npos = len(gts)
            # sort detections by decreasing confidence
            dects = sorted(dects, key=lambda conf: conf[2], reverse=True)
            TP = np.zeros(len(dects))
            FP = np.zeros(len(dects))
            # create dictionary with amount of gts for each image
            det = Counter([cc[0] for cc in gts])
            for key, val in det.items():
                det[key] = np.zeros(val)
            # print("Evaluating class: %s (%d detections)" % (str(c), len(dects)))
            # Loop through detections
            for d in range(len(dects)):
                # print('dect %s => %s' % (dects[d][0], dects[d][3],))
                # Find ground truth image
                gt = [gt for gt in gts if gt[0] == dects[d][0]]
                iouMax = sys.float_info.min
                for j in range(len(gt)):
                    # print('Ground truth gt => %s' % (gt[j][3],))
                    iou = bb_intersection_over_union(dects[d][3], gt[j][3])
                    # iou = Evaluator.iou(dects[d][3], gt[j][3])
                    if iou > iouMax:
                        iouMax = iou
                        jmax = j
                # Assign detection as true positive/don't care/false positive
                if iouMax >= IOUThreshold:
                    if det[dects[d][0]][jmax] == 0:
                        TP[d] = 1  # count as true positive
                        det[dects[d][0]][jmax] = 1  # flag as already 'seen'
                        # print("TP")
                    else:
                        FP[d] = 1  # count as false positive
                        # print("FP")
                # - A detected "cat" is overlaped with a GT "cat" with IOU >= IOUThreshold.
                else:
                    FP[d] = 1  # count as false positive
                    # print("FP")
            # compute precision, recall and average precision
            acc_FP = np.cumsum(FP)
            acc_TP = np.cumsum(TP)
            rec = acc_TP / npos
            prec = np.divide(acc_TP, (acc_FP + acc_TP))

            [ap, mpre, mrec, _] = Evaluator.ElevenPointInterpolatedAP(rec, prec)
            # add class result in the dictionary to be returned
            r = {
                "class": c,
                "precision": prec,
                "recall": rec,
                "AP": ap,
                "interpolated precision": mpre,
                "interpolated recall": mrec,
                "total positives": npos,
                "total TP": np.sum(TP),
                "total FP": np.sum(FP),
            }
            ret.append(r)
        return ret

    def PlotPrecisionRecallCurve(
        self,
        predictions,
        annotations,
        IOUThreshold=0.3,
        showAP=True,
        showInterpolatedPrecision=True,
        savePath="PrecisionRecallCurve",
    ):
        """PlotPrecisionRecallCurve
        Plot the Precision x Recall curve for a given class.
        Args:
            boundingBoxes: Object of the class BoundingBoxes representing ground truth and detected
            bounding boxes;
            IOUThreshold (optional): IOU threshold indicating which detections will be considered
            TP or FP (default value = 0.5);
            method (default = EveryPointInterpolation): It can be calculated as the implementation
            in the official PASCAL VOC toolkit (EveryPointInterpolation), or applying the 11-point
            interpolatio as described in the paper "The PASCAL Visual Object Classes(VOC) Challenge"
            or EveryPointInterpolation"  (ElevenPointInterpolation).
            showAP (optional): if True, the average precision value will be shown in the title of
            the graph (default = False);
            showInterpolatedPrecision (optional): if True, it will show in the plot the interpolated
             precision (default = False);
            savePath (optional): if informed, the plot will be saved as an image in this path
            (ex: /home/mywork/ap.png) (default = None);
            showGraphic (optional): if True, the plot will be shown (default = True)
        Returns:
            A list of dictionaries. Each dictionary contains information and metrics of each class.
            The keys of each dictionary are:
            dict['class']: class representing the current dictionary;
            dict['precision']: array with the precision values;
            dict['recall']: array with the recall values;
            dict['AP']: average precision;
            dict['interpolated precision']: interpolated precision values;
            dict['interpolated recall']: interpolated recall values;
            dict['total positives']: total number of ground truth positives;
            dict['total TP']: total number of True Positive detections;
            dict['total FP']: total number of False Negative detections;
        """

        english_tags = [
            "Homogeneous Liver",
            "Liver with Lesion",
            "Benign Lesion",
            "Malignant Lesion",
            "Cyst",
            "Angioma",
            "NFH",
            "Metastasis",
            "CHC",
            "Adenoma",
        ]
        results = self.GetPascalVOCMetrics(predictions, annotations, IOUThreshold)
        result = None
        # Each resut represents a class
        for result in results:
            classId = result["class"]
            precision = result["precision"]
            recall = result["recall"]
            average_precision = result["AP"]
            mpre = result["interpolated precision"]
            mrec = result["interpolated recall"]
            npos = result["total positives"]
            total_tp = result["total TP"]
            total_fp = result["total FP"]

            plt.close()
            if showInterpolatedPrecision:
                # Uncomment the line below if you want to plot the area
                # plt.plot(mrec, mpre, 'or', label='11-point interpolated precision')
                # Remove duplicates, getting only the highest precision of each recall value
                nrec = []
                nprec = []
                for idx in range(len(mrec)):
                    r = mrec[idx]
                    if r not in nrec:
                        idxEq = np.argwhere(mrec == r)
                        nrec.append(r)
                        nprec.append(max([mpre[int(id)] for id in idxEq]))
                plt.plot(nrec, nprec, "or", label="11-point interpolated precision")
            plt.plot(recall, precision, label="Precision")
            plt.xlabel("recall")
            plt.ylabel("precision")
            if showAP:
                ap_str = "{0:.2f}%".format(average_precision * 100)
                # ap_str = "{0:.4f}%".format(average_precision * 100)
                plt.title(
                    "Precision x Recall curve \nClass: %s, AP: %s"
                    % (str(english_tags[classId - 1]), ap_str)
                )
            else:
                plt.title(
                    "Precision x Recall curve \nClass: %s"
                    % str(english_tags[classId - 1])
                )
            plt.legend(shadow=True)
            plt.grid()
            if savePath is not None:
                plt.savefig(savePath + str(english_tags[classId - 1]) + ".png")
        return results

    @staticmethod
    def CalculateAveragePrecision(rec, prec):
        mrec = []
        mrec.append(0)
        [mrec.append(e) for e in rec]
        mrec.append(1)
        mpre = []
        mpre.append(0)
        [mpre.append(e) for e in prec]
        mpre.append(0)
        for i in range(len(mpre) - 1, 0, -1):
            mpre[i - 1] = max(mpre[i - 1], mpre[i])
        ii = []
        for i in range(len(mrec) - 1):
            if mrec[1:][i] != mrec[0:-1][i]:
                ii.append(i + 1)
        ap = 0
        for i in ii:
            ap = ap + np.sum((mrec[i] - mrec[i - 1]) * mpre[i])
        # return [ap, mpre[1:len(mpre)-1], mrec[1:len(mpre)-1], ii]
        return [ap, mpre[0 : len(mpre) - 1], mrec[0 : len(mpre) - 1], ii]

    @staticmethod
    # 11-point interpolated average precision
    def ElevenPointInterpolatedAP(rec, prec):
        # def CalculateAveragePrecision2(rec, prec):
        mrec = []
        # mrec.append(0)
        [mrec.append(e) for e in rec]
        # mrec.append(1)
        mpre = []
        # mpre.append(0)
        [mpre.append(e) for e in prec]
        # mpre.append(0)
        recallValues = np.linspace(0, 1, 11)
        recallValues = list(recallValues[::-1])
        rhoInterp = []
        recallValid = []
        # For each recallValues (0, 0.1, 0.2, ... , 1)
        for r in recallValues:
            # Obtain all recall values higher or equal than r
            argGreaterRecalls = np.argwhere(mrec[:] >= r)
            pmax = 0
            # If there are recalls above r
            if argGreaterRecalls.size != 0:
                pmax = max(mpre[argGreaterRecalls.min() :])
            recallValid.append(r)
            rhoInterp.append(pmax)
        # By definition AP = sum(max(precision whose recall is above r))/11
        ap = sum(rhoInterp) / 11
        # Generating values for the plot
        rvals = []
        rvals.append(recallValid[0])
        [rvals.append(e) for e in recallValid]
        rvals.append(0)
        pvals = []
        pvals.append(0)
        [pvals.append(e) for e in rhoInterp]
        pvals.append(0)
        # rhoInterp = rhoInterp[::-1]
        cc = []
        for i in range(len(rvals)):
            p = (rvals[i], pvals[i - 1])
            if p not in cc:
                cc.append(p)
            p = (rvals[i], pvals[i])
            if p not in cc:
                cc.append(p)
        recallValues = [i[0] for i in cc]
        rhoInterp = [i[1] for i in cc]
        return [ap, rhoInterp, recallValues, None]


def get_accuracy_well_localized(
    predictions: List[List[Dict[str, torch.Tensor]]],
    annotations: DatasetFasterRCNN,
    thresh_score: List[float],
    iou_th: float = 0.4,
):
    y_pred = []
    y_truth = []
    count = np.zeros(2)

    for k, prediction in enumerate(predictions):
        final_prediction = nms_perlabel(prediction, thresh_score)
        annotation = annotations[k][1]
        final_prediction["labels"] = [i if (i in [1, 2]) else (3 if i in [3,5,6,7] else 4) for i in final_prediction["labels"]]
        annotation["labels"] = torch.as_tensor(
            [i if (i in [1, 2]) else (torch.tensor(3) if i in [3,5,6,7] else torch.tensor(4)) for i in annotation["labels"]]
        )
        for j, boxe_truth in enumerate(annotation["boxes"]):
            if annotation['labels'][j] in [3,4]:
                count[annotation['labels'][j]-3] += 1
                for i, boxe_prediction in enumerate(final_prediction["boxes"]):
                    if final_prediction['labels'][i] in [3,4]:
                        box_a = boxe_truth.numpy()
                        box_b = boxe_prediction.numpy()
                        iou = bb_intersection_over_union(box_a, box_b)
                        if iou >= iou_th:
                            y_pred.append(final_prediction["labels"][i])
                            y_truth.append(annotation["labels"][j])
                            break
    matrix = confusion_matrix(y_truth, y_pred)
    print(
        "Accuracy per label for well localized boxes",
        matrix.diagonal() / matrix.sum(axis=1),
        flush=True,
    )
    print(count)
    print(matrix,flush=True)
    target_names  = [
        "Benign Lesion",
        "Malignant Lesion",
    ]
    print('Classification report',classification_report(y_truth, y_pred, target_names=target_names),flush=True)
    return matrix
