import math  # type: ignore
import sys  # type: ignore
from typing import Union

import torch
import torch.hub  # type: ignore
import torchvision  # type: ignore
from torch import nn
from torch.jit.annotations import Dict
from torch.utils.data import DataLoader
from torchvision.models.detection.faster_rcnn import \
    FastRCNNPredictor  # type: ignore

from detection.faster_rcnn import fasterrcnn_resnet50_fpn
from lib.vision_utils import MetricLogger  # type: ignore
from lib.vision_utils import SmoothedValue, reduce_dict, warmup_lr_scheduler


def train_one_epoch(
    model: torchvision.models.detection.FasterRCNN,
    optimizer: torch.optim.Optimizer,
    data_loader: DataLoader,
    device: torch.device,
    epoch: int,
    print_freq: int,
) -> MetricLogger:
    model.train()
    metric_logger = MetricLogger(delimiter="  ")
    metric_logger.add_meter("lr", SmoothedValue(window_size=1, fmt="{value:.6f}"))
    header = "Epoch: [{}]".format(epoch)

    lr_scheduler = None
    if epoch == 0:
        warmup_factor = 1.0 / 1000
        warmup_iters = min(1000, len(data_loader) - 1)
        lr_scheduler = warmup_lr_scheduler(optimizer, warmup_iters, warmup_factor)

    for images, targets in metric_logger.log_every(data_loader, print_freq, header):
        images = list(image.to(device) for image in images)
        targets = [{k: v.to(device) for k, v in t.items()} for t in targets]
        loss_dict: Dict[str, torch.Tensor] = model(images, targets)
        losses: torch.Tensor = sum(loss for loss in loss_dict.values())  # type: ignore
        model.train()
        loss_dict_reduced = reduce_dict(loss_dict)
        losses_reduced: Union[torch.Tensor, int] = sum(
            loss for loss in loss_dict_reduced.values()
        )
        loss_value = losses_reduced.item()  # type: ignore

        if not math.isfinite(loss_value):
            print("Loss is {}, stopping training".format(loss_value))
            print(loss_dict_reduced)
            sys.exit(1)

        optimizer.zero_grad()
        losses.backward()
        optimizer.step()

        if lr_scheduler is not None:
            lr_scheduler.step()

        metric_logger.update(loss=losses_reduced, **loss_dict_reduced)
        metric_logger.update(lr=optimizer.param_groups[0]["lr"])

    return metric_logger


def _get_iou_types(model):
    model_without_ddp = model
    if isinstance(model, torch.nn.parallel.DistributedDataParallel):
        model_without_ddp = model.module
    iou_types = ["bbox"]
    if isinstance(model_without_ddp, torchvision.models.detection.MaskRCNN):
        iou_types.append("segm")
    if isinstance(model_without_ddp, torchvision.models.detection.KeypointRCNN):
        iou_types.append("keypoints")
    return iou_types


def get_instance_segmentation_model(
    num_classes: int,
) -> torchvision.models.detection.fasterrcnn_resnet50_fpn:
    model = fasterrcnn_resnet50_fpn(pretrained=True)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model.to(device)
    # state_dict = torch.load("detection/backbone_fasterrcnn.pth", map_location="cpu")
    # model.backbone.load_state_dict(state_dict)
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)
    return model


def get_learnable_parameters(model, feature_extract):
    params_to_update = model.parameters()

    if feature_extract:
        params_to_update = []
        for name, param in model.named_parameters():
            if param.requires_grad:
                params_to_update.append(param)
                print("\t", name)
    return params_to_update
