import fiftyone as fo
import numpy as np
from PIL import Image


images_patt = 'path/to/your/images/'
path_val = 'path/to/your/annotation/txt/file/' #format : img_name label x1 y1 x2 y2
name_dataset = 'FLL-Test-Set'
label_field = "truth_flls_lesions"
already_created = True

def xyxy_to_xywh(xyxy):
    """Convert [x1 y1 x2 y2] box format to [x1 y1 w h] format."""
    if isinstance(xyxy, (list, tuple)):
        # Single box given as a list of coordinates
        assert len(xyxy) == 4
        x1, y1 = xyxy[0], xyxy[1]
        w = xyxy[2] - x1 + 1
        h = xyxy[3] - y1 + 1
        return (x1, y1, w, h)
    elif isinstance(xyxy, np.ndarray):
        # Multiple boxes given as a 2D ndarray
        return np.hstack((xyxy[:, 0:2], xyxy[:, 2:4] - xyxy[:, 0:2] + 1))
    else:
        raise TypeError('Argument xyxy must be a list, tuple, or numpy array.')

# Create dataset
if already_created:
    dataset = fo.load_dataset(name=name_dataset)
    dataset.delete()
    dataset = fo.Dataset(name=name_dataset)

else:
    dataset = fo.Dataset(name=name_dataset)

# Persist the dataset on disk in order to
# be able to load it in one line in the future
dataset.persistent = True
with open(path_val,'r') as read :
    for line in read :
        splitted = line.strip().split()
        img_path = images_patt + "data_images/" + splitted[0]
        img = Image.open(img_path).convert("RGB")
        height,width = img.size
        sample = fo.Sample(filepath=img_path)
        detections = []
        for obj_idx in range(int((len(splitted) - 1) / 5)):
            xmin = splitted[1 + 5 * obj_idx]
            ymin = splitted[2 + 5 * obj_idx]
            xmax = splitted[3 + 5 * obj_idx]
            ymax = splitted[4 + 5 * obj_idx]
            x,y,h,w =  xyxy_to_xywh([float(xmin), float(ymin), float(xmax), float(ymax)])
            bounding_box = [x/height,y/width,h/height,w/width]
            label = int(splitted[5 + 5 * obj_idx]) - 1
            detections.append(
                fo.Detection(label=str(label), bounding_box=bounding_box)
            )
        sample[label_field] = fo.Detections(detections=detections)
        dataset.add_sample(sample)

export_dir = "export/path/"

# Export the dataset
dataset.export(
    export_dir=export_dir,
    dataset_type=fo.types.COCODetectionDataset,
    label_field=label_field,
)

